# *Comparative study of C++ libraries for the finite element method* public documents

This repository contains the public versions of the documents generated within the "Comparative study of C++ libraries for the finite element method".

1. `Report.pdf`: The report text.
2. `Slides.pdf`: The slides of a presentation seminar about the project (July 15, 2020), in Spanish.
